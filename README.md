## Getting Started

First, install all dependencies.

```bash
npm i
```

And run the bloomreach backend.

Then, run the development server:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Docker

It's possible to use Docker to run this project. In that case, you don't need to install dependencies from package.json. Only [Docker Desktop](https://www.docker.com/products/docker-desktop) is needed.

First, start Docker Desktop. Then, you can start the project locally with `docker-compose up`. This will build the image if you haven't done this before (with `docker-compose build`) and start the server.

### upload an image to gcr

```
docker-compose build

# look what you made
docker images

# tag image with latest tag
# docker tag [SOURCE_IMAGE] [HOSTNAME]/[PROJECT-ID]/[IMAGE]
docker tag samenin-unive-spa_frontend gcr.io/showcase-samenin/samenin-unive-spa_frontend:latest

# push the image
docker push [HOSTNAME]/[PROJECT-ID]/[IMAGE]:[TAG]
docker push gcr.io/showcase-samenin/samenin-unive-spa_frontend:latest

# result
gcloud container images list-tags gcr.io/showcase-samenin/samenin-unive-spa_frontend

```

## Backend

You need to get access to this docker image.

pull docker image from grc `gcr.io/showcase-samenin/samenin`

for example using version v0.2.6

```bash
docker pull gcr.io/showcase-samenin/samenin:v0.2.6
```

Then you can start this image

```bash
docker run -dp 8080:8080 gcr.io/showcase-samenin/samenin:v0.2.6
```

Open [http://localhost:8080/cms](http://localhost:8080/cms) with your browser to see the result.


If you see a bloomreach login screen you can start the headless frontend server with `npm run dev`.

# Disqus

TODO:
The share link starts with http://localhost:3000