import React from 'react';
import { BrPageContext } from "@bloomreach/react-sdk";
import BlogItem from '../BlogItem';
import { container } from './styles.module.scss';

export default function Content ({ component }) {
  const models = component.getModels();

  // TODO: we need a better check I think
  if (!models && !models.document) return null;

  return (
    <BrPageContext.Consumer>
        {page => {
          const content = page.getContent(models.document)

          // authors array
          const authors = content.model.authors.map(authorModel => {
            return {
              author: page.getContent(authorModel),
              ...(authorModel.model && authorModel.image && { image: page.getContent(authorModel.model.image)})
            }
          });

          return (
            <div data-component={component.getName()} className={container}>
              <BlogItem content={content} authors={authors} />
            </div>
          )}
        }
    </BrPageContext.Consumer>
  );
}
