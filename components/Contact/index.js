import React from 'react';
import { BrPageContext, BrManageContentButton } from "@bloomreach/react-sdk";
import { container, contactImage, nonEscapedContent } from './styles.module.scss';

export default function Contact ({ component }) {
  const models = component.getModels();

  // limited error handling
  if (!models && !models.document) return null;

  return (
    <BrPageContext.Consumer>
        {page => {
          const content = page.getContent(models.document)
          if (!content) return null;
          const headerImage = page.getContent(content.model.image)
          return (
            <div data-component={component.getName()} className={container}>
              <BrManageContentButton content={content} />
              <img className={contactImage} src={headerImage.getUrl()}/>
              <div className={nonEscapedContent} dangerouslySetInnerHTML={{__html: content.model.contact.value}}/>
            </div>
          )}
        }
    </BrPageContext.Consumer>
  );
}
