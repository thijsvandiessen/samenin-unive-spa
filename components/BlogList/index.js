import React, { useEffect, useState } from 'react';
import Router from 'next/router';
import { BrPageContext } from "@bloomreach/react-sdk";
import classNames from 'classnames';
import BlogListItem from '../BlogListItem';
import { container, pagerContainer, pageHandlerButton, nextButton } from './styles.module.scss';
import { BrManageContentButton } from '@bloomreach/react-sdk';


export default function BlogList ({ component }) {
  const models = component.getModels();

  // TODO: we need a better check I think
  if (!models && !models.document) return null;

  const nextPageHandler = () => {
    // use the next/router to navigate
    Router.push({
      pathname: window.location.pathname,
      query: { page: models.pageable.nextPage },
    })
  }

  const pageHandler = (location) => {
    // we need a location
    if (!location) return;
    // use the next/router to navigate
    Router.push({
      pathname: window.location.pathname,
      query: { page: location },
    })
  }

  return (
    <BrPageContext.Consumer>
        {page => {
          return (
            <div data-component={component.getName()} className={container}>
              {models.pageable && models.pageable.items.map((item, idx) => {
                  const content = page.getContent(item)

                  // authors array
                  const authors = content.model.authors.map(authorModel => {

                    // if (authorModel.model) {
                    //   const test = page.getContent(authorModel.model.image)
                    // };

                    return {
                        author: page.getContent(authorModel),
                        ...(authorModel.model && {image: 'image'})
                      }
                  });

                  return(
                    <BlogListItem key={idx} content={content} authors={authors} />
                  )
              })}

              <div className={pagerContainer}>
                {models.pageable && models.pageable.showPagination && models.pageable.previousPage && (
                <button
                  className={classNames({
                    [pageHandlerButton]: pageHandlerButton,
                  })}
                  type="button"
                  onClick={() => pageHandler(models.pageable.previousPage)}
                >
                  Vorige pagina
                </button>)}
                {models.pageable && models.pageable.showPagination && models.pageable.nextPage && (
                  <button 
                    className={classNames({
                      [pageHandlerButton]: pageHandlerButton,
                      [nextButton]: nextButton,
                    })}
                    type="button" 
                    onClick={() => pageHandler(models.pageable.nextPage)}
                  >
                    Volgende pagina
                  </button>
                )}
              </div>
            </div>
          )}
        }
    </BrPageContext.Consumer>
  );
}
