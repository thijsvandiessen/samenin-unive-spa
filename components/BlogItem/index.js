import React from 'react';
import { blogListItem } from './styles.module.scss';
import { BrManageContentButton } from '@bloomreach/react-sdk';
import { DiscussionEmbed } from 'disqus-react';

// a component used in a bloglist or content component
export default function BlogItem ({ content, authors }) {

  if (!content) return null;

  // date options
  const options = { year: 'numeric', month: 'long', day: 'numeric' };

  // publication date
  const date = new Date(content.model.publicationdate)
    .toLocaleDateString(content.model.localeString, options)

  console.log(content)

  return(
    <>
    <article className={blogListItem}>
      <h2>{content.model.title}</h2>
      <p>{date}</p>
      <p>
        {authors && 'Geschreven door: '}
        {authors && authors.map(({ author }, authorIdx) => {
        return (
          author &&
          author.model &&
          author.model.fullName &&  
          <span key={authorIdx}>{author.model.fullName} {' '}</span>
        )
      })}
      </p>
      <p><em>{content.model.introduction}</em></p>
      <hr/>
      <div dangerouslySetInnerHTML={{__html: content.model.content.value}}/>
      <BrManageContentButton content={content} />
    </article>
    </>
  )
};
