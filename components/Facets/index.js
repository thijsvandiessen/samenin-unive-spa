import React from 'react';
import { BrPageContext } from "@bloomreach/react-sdk";

export default function Facets ({ component }) {
  const models = component.getModels();
  // console.log(component)
  // console.log(component.getParameters())
  // console.log(models)
  return (
    <div data-component={component.getName()}>
      <BrPageContext.Consumer>
          {page => {
            const content = page.getContent(models.facets);

            // console.log(content);

            // if hidden
            if (!content) return null;

            return (
              <div>
                {/* facets */}
              </div>
            )}
          }
      </BrPageContext.Consumer>
    </div>
  );
}
