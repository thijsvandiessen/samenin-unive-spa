import React from 'react';
import Router from 'next/router';

import { BrPageContext } from "@bloomreach/react-sdk";
import classNames from 'classnames';
import { articleList, article, articleImage, link, text, tagContainer, tag, metadata, dateStyle, pagerContainer, pageHandlerButton, nextButton } from './styles.module.scss';


export default function ArticleList ({ component }) {
  const models = component.getModels();

  const pageHandler = (location) => {
    // we need a location
    if (!location) return;
    // use the next/router to navigate
    Router.push({
      pathname: window.location.pathname,
      query: { page: location },
    })
  }

  return (
    <BrPageContext.Consumer>
      {page => 
        <div data-component={component.getName()} className={articleList}>
          {models.pageable && models.pageable.items.map((item, itemIndex) => {
            const content = page.getContent(item);

            // get the header image
            const image = page.getContent(content.model.image);

            // date options
            const options = { year: 'numeric', month: 'long', day: 'numeric' };

            // publication date
            const date = new Date(content.model.publicationdate).toLocaleDateString(content.model.localeString, options);

            return (
              <section className={article} key={itemIndex}>
                <a className={link} href={content.getUrl()} title="Lees meer">
                  {image && image.getUrl() && <div className={articleImage}>
                    <img src={image.getUrl()} />
                  </div>}
                  <div className={text}>
                    <div className={metadata}>
                      <div className={tagContainer}>
                        {content.model.categorie && content.model.categorie.map((categorie, keyIndex) => <span className={tag} key={keyIndex}>{categorie}</span>)}
                      </div>
                      <p className={dateStyle}>{date}</p>
                    </div>
                    <h2>{content.model.title}</h2>
                    {itemIndex === 0 && content.model.summary && <p><em>{content.model.summary}</em></p>}
                  </div>
                </a>
              </section>
            )
          })}

          <div className={pagerContainer}>
            {models.pageable && models.pageable.showPagination && models.pageable.previousPage && (
              <button
                className={classNames({
                  [pageHandlerButton]: pageHandlerButton,
                })}
                type="button"
                onClick={() => pageHandler(models.pageable.previousPage)}
              >
                Vorige pagina
              </button>)}
            {models.pageable && models.pageable.showPagination && models.pageable.nextPage && (
              <button 
                className={classNames({
                  [pageHandlerButton]: pageHandlerButton,
                  [nextButton]: nextButton,
                })}
                type="button" 
                onClick={() => pageHandler(models.pageable.nextPage)}
              >
                Volgende pagina
              </button>
            )}
          </div>
        </div>
      }
    </BrPageContext.Consumer>
  )
};
