import React from 'react';
import { card, contentBox, headerImage } from './newsCard.module.scss';

export default function Card ({ title, text, bright, index }) {
  return (
    <div className={card}>
      <img
        className={headerImage}
        src="https://via.placeholder.com/150"
        alt="placeholder"
      />
      <div className={contentBox}>
        <p>Laatste nieuws 04 mei 2020</p>
        <h3>Hooibroei voorkomen begint al tijdens het maaiseizoen</h3>
        <p>In het maaiseizoen is het voor veehouders belangrijk om het versgemaaide gras zo droog en veilig mogelijk op te slaan. Niemand zit immers op brand door hooibroei te wachten. Taeke Oldenburger is één van de technisch inspecteurs die namens Univé boeren helpt met gerichte adviezen om de kans op brand door hooibroei eenvoudig te verkleinen.</p>
      </div>
    </div>
  );
}
