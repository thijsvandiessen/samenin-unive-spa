import React from 'react';
import { BrPageContext } from "@bloomreach/react-sdk";
import { article, articleImage, metadata, tagList, tag, dateStyle, title, abstract, escapedContent } from './styles.module.scss';
import { DiscussionEmbed } from 'disqus-react';

export default function ArticleList ({ component }) {
  const models = component.getModels();

  return (
    <BrPageContext.Consumer>
      {page => {
        const content = page.getContent(models.document);

        // get the header image
        const image = page.getContent(content.model.image);

        // date options
        const options = { year: 'numeric', month: 'short', day: 'numeric' };

        // publication date
        const date = new Date(content.model.publicationdate).toLocaleDateString(content.model.localeString, options);

        console.log(content)

        return (
          <>
            <article className={article}>
              {image && image.getUrl() && <img className={articleImage} src={image.getUrl()} />}
              <div className={metadata}>
                <div className={tagList}>
                  {content && content.model.categorie && (content.model.categorie === typeof Array)
                    ? content.model.categorie.map((categorie, categorieIndex) => <span key={categorieIndex} className={tag}>{categorie}</span>)
                    : <span className={tag}>{content.model.categorie}</span>
                  }
                </div>
                <p className={dateStyle}>{date}</p>
              </div>

              <h1 className={title}>{content.model.title}</h1>
              {content.model.summary && <p className={abstract}>{content.model.summary}</p>}
              <hr />
              <div className={escapedContent} dangerouslySetInnerHTML={{__html: content.model.content.value}}/>
            </article>
            <DiscussionEmbed
              className={abstract}
              shortname='samenin'
                config={{
                  url: 'http://localhost:3000' + '/articles/' + content.model.name,
                  identifier: content.model.id,
                  title: content.model.title,
                  language: 'nl' //e.g. for Traditional Chinese (Taiwan)	
                }
              }
              />
            </>
        )
      }}
    </BrPageContext.Consumer>
  );
}
