
export default function SnelPremieBerekenen({ children, component, page }) {
  const {
    ctaText, 
    ctaUrl, 
    designVariant, 
    document, 
    documentPath, 
    linkToDocument, 
    moreInfoText, 
    moreInfoUrl, 
    startNewZebra, 
    text, 
    title, 
    useZebra,
  } = component.getParameters();

  return (
    <div data-component={component.getName()}>
      {title && <h2>{title}</h2>}
      {ctaText && ctaUrl && (
        <p>
          <a href={ctaUrl}>{ctaText}</a>
        </p>
      )}
      {moreInfoText && moreInfoUrl && (
        <p>
          <a href={moreInfoUrl}>{moreInfoText}</a>
        </p>
      )}
    </div>
  )
};
