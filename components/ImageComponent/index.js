import React from 'react';
import { container, image } from './styles.module.scss'

export default function ImageComponent ({ component }) {

  const { document } = component.getParameters()

  if (!document) return null;

  return (
    <div className={container} data-component={component.getName()} >
      <img className={image} src={`http://localhost:8080/site/binaries${document}`}/>
    </div>
  );
}
