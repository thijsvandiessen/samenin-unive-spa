import React from 'react';
import { footer, footerText, importantLinks } from './styles.module.scss';

export default function Footer ({component}) {

  // console.log(component)

  const { menu } = component.getModels();

  // if no menu items, do not render
  if (!menu || !menu.siteMenuItems) return null;
  
  // console.log(menu)

  return (
    <footer className={footer}> 
      {/* <hr />
      <div className={footerText}>
        copyright
      </div> */}
      <hr/>
      <ul>
        {menu.siteMenuItems.map((item, idx) => {
          // check if a menu item is undefiend
          if (!item._links.site) return;
          return (
            <li key={idx}>
              <a
                className={classNames({
                  [menuItem]: menuItem,
                  [selected]:
                    item.name ===
                    (menu.selectSiteMenuItem && menu.selectSiteMenuItem.name),
                })}
                href={
                  item._links.site.href.replace(regex, "")
                    ? item._links.site.href.replace(regex, "")
                    : "/"
                }
              >
                {item.name === 'Home' ? <Logo /> : item.name}
              </a>
            </li>
          )
        })}
      </ul>
      <ul className={importantLinks}>
        <li>disclaimer</li>
        <li>cookies</li>
        <li>privacy</li>
      </ul>
    </footer>
  );
}
