import React, {useState, useEffect} from 'react';
import Router from 'next/router'
import { BrPageContext } from "@bloomreach/react-sdk";
import { container, searchContainer } from './styles.module.scss';


export default function Search ({ component }) {
  const [value, setValue] = useState();

  useEffect(() => {
    const paramsString = window.location.search;
    const searchParams = new URLSearchParams(paramsString);

    // only use the value of query
    if (!searchParams.has("query")) return;
    
    const urlSearchValue = searchParams.get("query").replace(/\+/g, ' ')

    setValue(() => decodeURIComponent(urlSearchValue.replace(/%2B/g, ' ')))

  },[])

  const models = component.getModels();

  const onSubmit = (data) => {
    data.preventDefault()

    console.log(data.target.zoek.value)
    // encode the search value
    const encodedUrlParameter = encodeURIComponent(data.target.zoek.value).replace(/%20/g, "+")

    console.log(encodedUrlParameter)

    // use the next/router to navigate
    Router.push({
      pathname: window.location.pathname, 
      query: { query: encodedUrlParameter },
    })
  }

  return (
    <div data-component={component.getName()}>

      <BrPageContext.Consumer>
          {page => {
            return (
              <div className={container}>
                <form className={searchContainer} onSubmit={onSubmit}>
                  <input name="zoek" defaultValue={value} type="text"></input>
                  {/* <div dangerouslySetInnerHTML={{__html: contactPage.model.contact.value}}/> */}
                  <button type="submit">zoek</button>
                </form>
                {value && <p>Resultaten voor: <em>{value}</em></p>}
              </div>
            )}
          }
      </BrPageContext.Consumer>
    </div>
  );
}
