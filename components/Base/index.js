import React, { useState, useEffect } from 'react';
import { withRouter } from 'next/router'
import axios from 'axios';
import { BrPage, BrComponent } from '@bloomreach/react-sdk';
import componentsMap from '../../utils/componentsMap';
import getCookieValue from '../../utils/getCookieValue';

// Most important component of this site.
// It initializes the connection with bloomreach
function Base({ router }) {
  const [token, setToken] = useState(false);

  // we can set a token when we hava it
  if (router.query.token) {
    // We should set a cookie now with the new token value
    document.cookie = `token=${router.query.token}`;
  }

  useEffect(() => {
    // don't use the token when we are not using a cms
    // TODO: better check?
    if (window.location.ancestorOrigins.length === 0) return;

    // a way to get the initial token
    const searchParams = new URLSearchParams(window.location.search);
    const paramToken = searchParams.get("token");

    if (paramToken) {
      return setToken(() => paramToken);
    }

    // A failsafe option
    // another way to get a token (this failed sometimes...)
    if (router.query.token) {
      // and set the new token as local state
      return setToken(() => router.query.token)
    }

    // if we still do not have a token
    if (!token) {
      // Get and set the token
      setToken(getCookieValue('token'))
    }
      
  },[])

  // At first render: this component does not return the router.asPath correctly
  if(router.asPath === '/[...slug]') return null;

  // initialize bloomreach with this config
  const config = {
    httpClient: axios,
    cmsBaseUrl: 'http://localhost:8080/site/sameninnunspeet',
    request: {
      path: router.asPath,
      ...(token && { 
        headers: { 
          Authorization: `Bearer ${token}`,
        },
      }),
    },
  }

  // a check if backend runs
  fetch(config.cmsBaseUrl, {
    method: 'HEAD'
  }).catch((error) => {
    console.error('The backend is possibly not running:', error);
  });

  return (
    <BrPage configuration={config} mapping={componentsMap}>
      <BrComponent path="header" />
      <BrComponent path="menu" />
      <BrComponent path="main" />
      <BrComponent path="footer" />
      <BrComponent path="footer-menu-socials-and-legal" />
    </BrPage>
  )
}

export default withRouter(Base);
