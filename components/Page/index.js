import React from 'react';
import Head from 'next/head';
import { BrComponent, BrPageContext } from '@bloomreach/react-sdk';
import componentsMap from '../../utils/componentsMap';

export default function Page ({ component }) {
  const children = component.getChildren();
  return (
    <div data-component={component.getName()}>
      <BrPageContext.Consumer>
      {page => {
        return (
          <Head>
            <title>{page.getTitle() || 'default page title'}</title>
            <meta
              name='description'
              content='global site description'
            />
          </Head>
        )}
      }
    </BrPageContext.Consumer>
      {children.map((child, idx) => {
        const Component = componentsMap[child.getName()];
        return (
          <BrComponent path={child.getName()} key={idx}>
            <Component component={child} />
          </BrComponent>
        )}
      )}
    </div>
  );
}
