import React from 'react';
import { BrComponent } from '@bloomreach/react-sdk';
import componentsMap from '../../utils/componentsMap';

export default function Main ({ component }) {
  const children = component.getChildren();

  return (
    <main data-component={component.getName()}>
      {children.map((child, idx) => {
        const Component = componentsMap[child.getName()];
        return (
          <BrComponent path={child.getName()} key={idx}>
            <Component component={child} />
          </BrComponent>
        )}
      )}
    </main>
  );
}
