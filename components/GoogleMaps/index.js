import React from 'react';
import { Client, Status } from "@googlemaps/google-maps-services-js";
import axios from 'axios';
// import { } from './styles.module.scss';

export default function GoogleMaps ({component}) {

  const client = new Client();

  const params = component.getParameters()
 
  client
    .elevation({
      params: {
        locations: [{ lat: 45, lng: -110 }],
        key: params.apiKey
      },
      timeout: 1000 // milliseconds
    }, axios)
    .then(r => {
      console.log(r.data.results[0].elevation);
    })
    .catch(e => {
      console.log(e);
    });

  return (
    <div> 
      google maps container
    </div>
  );
}
