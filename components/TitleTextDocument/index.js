import { BrPageContext, BrManageContentButton } from "@bloomreach/react-sdk";
import { container } from './styles.module.scss'

export default function TitleTextDocument({ component }) {
  const parameters = component.getParameters();
  const models = component.getModels();

  return (
    <div data-component={component.getName()} className={container}>
      {/* TODO: communicate to backend that this is confusing in a document. */}
      {parameters.title && <h2>{parameters.title}</h2>}
      {parameters.text && <p>{parameters.text}</p>}

      <BrPageContext.Consumer>
          {page => {
            const content = page.getContent(models.document)
            if (!content || !content.model.link) return null;
            const link = page.getContent(content.model.link.internalUrl)
            return (
              <div data-document={content.model.name}>
                <BrManageContentButton content={content} />
                <h1>{content.model.title}</h1>
                <div>
                  {content.model.keys &&
                    content.model.keys.map((key, keyIndex) =>
                      <span key={keyIndex}>{key}</span>
                    )
                  }
                </div>
                <p>
                  <strong>{content.model.introduction}</strong>
                </p>
                <div dangerouslySetInnerHTML={{__html: content.model.content.value}}/>
                <a 
                  href={link.getUrl().substring(40)} 
                  target={content.model.link.newTab ? '_blank' : '_self'}
                >
                  {content.model.link.urlText}
                </a>
              </div>
            )}
          }
      </BrPageContext.Consumer>
    </div>
  )
};
