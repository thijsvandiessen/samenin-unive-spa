import React from 'react';
import { blogListItem, readMore } from './styles.module.scss';
import { BrManageContentButton } from '@bloomreach/react-sdk';

// a component used in a bloglist or content component
export default function BlogListItem ({ content, authors }) {

  if (!content) return null;

  // date options
  const options = { year: 'numeric', month: 'long', day: 'numeric' };

  // publication date
  const date = new Date(content.model.publicationdate)
    .toLocaleDateString(content.model.localeString, options)
  
  return(
    <article className={blogListItem}>
      <h2>{content.model.title}</h2>
      <p>{date}</p>
      {authors && authors.map(({ author }, authorIdx) => author &&
        author.model &&
        author.model.fullName &&
        <p key={authorIdx}>Geschreven door: {author.model.fullName}</p>
      )}
      <p><em>{content.model.introduction}</em></p>
      {content && content.getUrl() && <a className={readMore} href={content.getUrl()}>Lees meer</a>}
      <BrManageContentButton content={content} />
    </article>
  )
};
