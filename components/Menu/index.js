import React from 'react';
import classNames from 'classnames';
import { BrManageMenuButton } from '@bloomreach/react-sdk';
import { navigation, menuItem, selected, navList } from './styles.module.scss';
import Logo from '../Logo';

export default function Menu({ component }) {
  const { menu } = component.getModels();

  // if no menu items, do not render
  if (!menu || !menu.siteMenuItems) return null;

  // TODO: this is a stupid hack...
  const regex = /\/site\/sameninnunspeet/gi;

  return (
    <nav data-component={component.getName()} className={navigation}>
      <BrManageMenuButton menu={menu} />
      <ul className={navList}>
        {menu.siteMenuItems.map((item, idx) => {
          // check if a menu item is undefiend
          if (!item._links.site) return;

          // console.log(item)
          return (
            <li key={idx}>
              <a
                className={classNames({
                  [menuItem]: menuItem,
                  [selected]:
                    item.name ===
                    (menu.selectSiteMenuItem && menu.selectSiteMenuItem.name),
                })}
                href={
                  item._links.site.href.replace(regex, "")
                    ? item._links.site.href.replace(regex, "")
                    : "/"
                }
              >
                {item.name === 'Home' ? <Logo /> : item.name}
              </a>
            </li>
          )
        })}
      </ul>
    </nav>
  );
};
