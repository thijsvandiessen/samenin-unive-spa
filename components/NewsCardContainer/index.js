import React from 'react';
import { container } from './newsCardContainer.module.scss';

export default function NewsCardContainer ({ children }) {
  return (
    <div className={container}>
      {children}
    </div>
  );
}
