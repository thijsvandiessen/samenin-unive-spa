
export default function DocumentList({children, component, page}) {
  const {
    designVariant, 
    document,
    documentPath,
    linkToDocument,
    startNewZebra,
    text,
    title,
    useZebra,
  } = component.getParameters();

  return (
    <div data-component={component.getName()}>
      {title && <h2>{title}</h2>}
      {text && <p>{text}</p>}
    </div>
  )
};
