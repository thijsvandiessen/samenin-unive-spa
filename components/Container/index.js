import React from 'react';
import componentsMap from '../../utils/componentsMap';

export default function Container ({ component }) {
  const children = component.getChildren();
  return (
    <div data-component={component.getName()}>
      {children.map((child, idx) => {
        const Component = componentsMap[child.getName() || 'test'];
        return (
          <Component key={idx} component={child} />
        )}
      )}
    </div>
  )
}
