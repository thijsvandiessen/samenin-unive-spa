import { container } from './styles.module.scss'

export default function TitleTextComponent({ component }) {
  const parameters = component.getParameters();
  return (
    <div data-component={component.getName()} className={container}>
      {parameters.title && <h2>{parameters.title}</h2>}
      {parameters.text && <p>{parameters.text}</p>}
    </div>
  )
};
