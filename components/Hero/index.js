export default function Hero({ component }) {
  const { title, ctaUrl, ctaText, heroImage } = component.getParameters();

  if (!heroImage) return null;
  return (
    <div data-component={component.getName()}>
      {title && <h2>{title}</h2>}
      {ctaText && <a href={ctaUrl}>{ctaText}</a>}
      {heroImage && <img src={`http://localhost:8080/site/binaries${heroImage}`} />}
    </div>
  )
};
