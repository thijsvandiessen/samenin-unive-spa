import React from 'react';
import { BrPageContext } from "@bloomreach/react-sdk";
import classNames from 'classnames';
import { banner, bannerImage, bannerText, block } from './styles.module.scss';

export default function Banner ({ component }) {
  const models = component.getModels();
  if (!models && !models.document) return null;
  return (
    <BrPageContext.Consumer>
      {page => {

        const pageContent = page.getContent(models.document)

        // we can have no page content...
        if (!pageContent) return null;

        const image = page.getContent(pageContent.model.image)

        return (
          <div className={banner} data-component={component.getName()}>
            <div className={classNames({[bannerText]: bannerText, [block]: block})}>
              <h1>{pageContent.model.title}</h1>
              <div dangerouslySetInnerHTML={{__html: pageContent.model.content.value}}/>
            </div>
            <div className={block}>
              <img className={bannerImage} src={image.getUrl()} />
            </div>
          </div>
        )}
      }
    </BrPageContext.Consumer>
  );
}
