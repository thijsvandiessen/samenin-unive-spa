
const getCookieValue = (name) => {
  // if no document object
  if (!document) return;

  // no cookies exist
  if (!document.cookie) return;

  // get a cookies array
  const cookies = document.cookie.split('; ')

  // get the specific cookie by name
  const cookie = cookies.find(row => row.startsWith(name))

  // if it does not exists
  if (!cookie) return

  // get the value
  const value = cookie.split('=')[1];

  return value
}

export default getCookieValue;
