import ArticleList from '../components/ArticleList';
import Article from '../components/Article';
import Banner from '../components/Banner';
import BlogList from '../components/BlogList'
import Contact from '../components/Contact';
import Container from '../components/Container';
import Content from '../components/Content';
import Facets from '../components/Facets';
import Main from '../components/Main';
import Header from '../components/Header';
import Footer from '../components/Footer'
import Menu from '../components/Menu';
import Hero from '../components/Hero';
import Page from '../components/Page';
import GoogleMaps from '../components/GoogleMaps';
import SnelPremieBerekenen from '../components/SnelPremieBerekenen';
import Search from '../components/Search';
import TitleText from '../components/TitleText';
import TitleTextDocument from '../components/TitleTextDocument';
import DocumentList from '../components/DocumentList';
import ImageComponent from '../components/ImageComponent';

function Test ({component}) {
  // console.log('test-start-----------------------')
  // console.log(component)
  // console.log(component.getName())
  // console.log(component.getParameters())
  // console.log('test-end-----------------------')
  return <div data-component={component.getName()}>component: "{component.getName()}" is not configured yet :-)</div>
}

const componentsMap = {
  'Article List': ArticleList,
  'Article': Article,
  'container': Container,

  'menu': Menu,
  'header': Header,
  'footer': Footer,
  'footer-menu-socials-and-legal': Footer,
  'hero': Hero,

  'main': Main,

  'spb': SnelPremieBerekenen,
  'SPB (Snel premie berekenen)' : SnelPremieBerekenen,

  'Titel/Tekst' : TitleTextDocument,
  // 'titletextdocument': TitleTextDocument,
  // TODO: do we need to remove this double component?
  'documentlist': DocumentList,
  'Document List' : DocumentList,
  'imagecomponent': ImageComponent,
  'Image': ImageComponent,
  // 'banner': Banner,
  'Banner': Banner,

  // 'bloglist': BlogList,
  'Blog List': BlogList,

  // I don't understand this component
  'Facets': Facets,

  'Google Maps': GoogleMaps,
  'OpenStreetMap': Test,
  'contact-component': Contact,
  'Contact Info': Contact,
  // 'openstreetmap': Test,
  'carousel': Test,
  'Carousel': Test,
  'blogauthorposts': Test,
  'Blog Posts by Author': Test,
  'Search Box': Search,
  'test': Test,
  'Content': Content,

  // todo: general error component that can display an undefiend name
}

export default componentsMap;